<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(
            'users',
            function ($table) {
                $table->increments('id');
                $table->boolean('root')->default(false);
                $table->string('role', 10)->default('guest');
                $table->string('name', 64);
                $table->string('username', 64);
                $table->string('password', 64);
                $table->string('email', 100)->unique();
                //$table->integer('remember_token ');
                $table->boolean('active')->default(true);
                $table->timestamps();
            }
        );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('users');
    }

}
