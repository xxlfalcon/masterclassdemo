<?php

/**
 * Actors model config
 */

return array(

    'title' => 'Users',

    'single' => 'user',

    'model' => 'User',

    /**
     * The display columns
     */
    'columns' => array(
        'id',
        'root' => array(
            'title' => 'Root',
            'select' => "(:table).root",
        ),
        'role' => array(
            'title' => 'Role',
            'select' => "(:table).role",
        ),
        'name' => array(
            'title' => 'Name',
            'select' => "(:table).name",
        ),
        'email' => array(
            'title' => 'Email',
            'select' => "(:table).email",
        ),
        'active' => array(
            'title' => 'Active',
            'select' => "(:table).active",
        ),
    ),

    /**
     * The filter set
     */
    'filters' => array(
        'id',
        'role' => array(
            'title' => 'Role',
        ),
        'name' => array(
            'title' => 'Name',
        ),
        'email' => array(
            'title' => 'Email',
        ),
        'active' => array(
            'title' => 'Active',
        ),
    ),

    /**
     * The editable fields
     */
    'edit_fields' => array(
        'name' => array(
            'title' => 'Name',
            'type' => 'text',
        ),
        'email' => array(
            'title' => 'Email',
            'type' => 'text',
        ),
        'active' => array(
            'title' => 'Active',
            'type' => 'bool',
        ),
    ),

);