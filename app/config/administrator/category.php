<?php

/**
 * Actors model config
 */

return array(

    'title' => 'Categories',

    'single' => 'category',

    'model' => 'Category',

    /**
     * The display columns
     */
    'columns' => array(
        'id',
        'name' => array(
            'title' => 'Name',
            'select' => "(:table).name",
        ),
        'active' => array(
            'title' => 'Active',
            'select' => "(:table).active",
        ),
    ),

    /**
     * The filter set
     */
    'filters' => array(
        'name' => array(
            'title' => 'Name',
        ),
        'active' => array(
            'title' => 'Active',
        ),
    ),

    /**
     * The editable fields
     */
    'edit_fields' => array(
        'name' => array(
            'title' => 'Name',
            'type' => 'text',
        ),
        'active' => array(
            'title' => 'Active',
            'type' => 'bool',
        ),
    ),

);