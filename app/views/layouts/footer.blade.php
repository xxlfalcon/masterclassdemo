@section('head')
@parent
{{ HTML::style('css/sticky-footer.css') }}
@stop

<div id="footer">
    <div class="container">
        <p class="text-muted">StudyX Post System.</p>
    </div>
</div>