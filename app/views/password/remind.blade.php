@extends('layouts.bootstrap')

@section('title')
{{ Lang::get('password.remind') }}
@stop

@section('content')

<h1>{{ Lang::get('password.remind') }}</h1>
<form action="{{ action('RemindersController@postRemind') }}" method="POST">
    <input type="email" name="email">
    <input type="submit" value="Send Reminder">
</form>

@stop