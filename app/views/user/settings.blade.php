@extends('layouts.bootstrap')

@section('title')
User settings
@stop

@section('content')

<h1>User Settings</h1>

<div class="jumbotron text-center">
    <h2>{{ $user->name }}</h2>
    <p>
        <strong>Email:</strong> {{  $user->email }}<br>
        <strong>Role:</strong> {{ $user->role }}
    </p>
</div>
<div class="jumbotron text-left">
    <h2>Last posts</h2>
    <ul>
    @foreach($user->posts as $post)
        <li><a href="{{ URL::route( 'posts.show', array('id' => $post->id)) }}">{{ $post->title }}</a> ({{ $post->getCommentCount()}})</li>
    @endforeach
    </ul>
    <button type="button" class="btn btn-success">Clear my Posts</button>
</div>

<div class="jumbotron text-left">
    <h2>Last comments</h2>
    <ul>
        @foreach($user->getComments() as $comment)
        <li><a href="{{ URL::route( 'comments.show', array('id' => $comment->id)) }}">{{ $comment->content }}</a></li>
        @endforeach
    </ul>
    <button type="button" class="btn btn-success">Clear my Comments</button>
</div>

@stop