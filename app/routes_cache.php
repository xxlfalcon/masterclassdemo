
<?php

Route::get(
    'cache/all',
    function () {
        $value = Cache::all();
        var_dump($value);
        // return Redirect::route('home');
    }
);

Route::get(
    'cache/put/{key}/{value}',
    function ($key, $value) {
        $expiresAt = Carbon::now()->addMinutes(5);
        Cache::put($key, $value, $expiresAt);
        return Redirect::route('home');
    }
);

Route::get(
    'cache/get/{key}',
    function ($key) {
        $value = Cache::get($key);
        return "The values from cache: $value";
        //return Redirect::route('home');
    }
);

Route::get(
    'cache/flush',
    function () {
        Cache::flush();
        return Redirect::route('home');
    }
);

Route::get(
    'cache/forever/{key}/{value}',
    function ($key, $value) {
        Cache::forever($key, $value);
        return Redirect::route('home');
    }
);

Route::get(
    'cache/remember/{key}/{value}',
    function ($key, $value) {
        Cache::remember($key, $value);
        return Redirect::route('home');
    }
);

Route::get(
    'cache/increment/{key}/{value}',
    function ($key, $value = 1) {
        Cache::increment($key, $value);
        return Redirect::route('home');
    }
);

Route::get(
    'cache/decrement/{key}/{value}',
    function ($key, $value = 1) {
        Cache::decrement($key, $value);
        return Redirect::route('home');
    }
);