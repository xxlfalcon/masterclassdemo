<?php
/**
 * Created by PhpStorm.
 * User: Harry van der Valk @ HVSoftware
 * Date: 18-5-14
 * Time: 16:36
 */

class UserController extends \BaseController {

    public function settings() {
        $user = Auth::user();
        return View::make('user.settings', compact('user'));
    }

    public function register() {
        return View::make('auth.register');
    }

    public function registerHandle($name, $email) {
        return View::make('home');
    }

    public function requestPassword() {
        return View::make('auth.requestPassword');
    }

    public function requestPasswordHandle($email) {
        $user = User::findBy($email);
        if (!$user) {
            //return Redirect::404();
        }
        $user->password = Hash::make('secret');
        $user::save();

//        Mail::send('password.request', $data, function($message)
//        {
//            $message->to('foo@example.com', 'John Smith')->subject('Welcome!');
//        });

        return View::make('home');
    }
} 