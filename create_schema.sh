#php artisan generate:scaffold tag --fields="title:string"
#php artisan generate:scaffold category --fields="name:string, active:boolean"
php artisan generate:scaffold comment --fields="content:string, active:boolean"
php artisan generate:scaffold post --fields="title:string, content:string, author_id:integer"
php artisan generate:pivot post comment
php artisan generate:resource postEvent --fields="post_id:integer, action:string, content:string, type:integer"
php artisan generate:pivot post category
